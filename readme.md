# karmabot

## requirements
* python3.8
* [pdm][pdm] (`brew install pdm`)
* npm

## getting started
```bash
# install reqs
pdm install --dev

# list development commands
pdm run -l
```

[pdm]: https://pdm.fming.dev
