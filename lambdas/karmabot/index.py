import random
import re
import sys
from ssl import SSLContext
from typing import *

import dataset
from apig_wsgi import make_lambda_handler
from loguru import logger
from pydantic import BaseModel
from slack_sdk import WebClient
from slackeventsapi import SlackEventAdapter
from tenacity import retry, stop_after_attempt, wait_fixed

from config import Config

config = Config()


class User(BaseModel):
    id: str
    display_name: str
    karma: int = 0


logger.add(sys.stderr, serialize=True)

db = dataset.connect(
    f"mysql+auroradataapi://:@/{config.database}",
    engine_kwargs=dict(
        echo=config.echo_db,
        connect_args=dict(
            aurora_cluster_arn=config.cluster_arn,
            secret_arn=config.cluster_secret_arn,
        ),
    ),
)

user_table: dataset.Table = db.create_table(
    "user", primary_id="id", primary_type=db.types.string(20)
)

timestamp_table: dataset.Table = db.create_table(
    "timestamp", primary_id="timestamp", primary_increment=False
)

slack_events_adapter = SlackEventAdapter(
    config.slack.signing_secret, endpoint=config.slack.endpoint
)

app = slack_events_adapter.server


handler = make_lambda_handler(app)

client = WebClient(config.slack.access_token, ssl=SSLContext())

PATTERN = re.compile(r"<@(\w*)>\s?((\+|-){2,})")


def split(string) -> Iterator[Tuple[str, str]]:

    recipient_id_pattern = r"[A-Z0-9]{9,12}"

    plus_or_minus_pattern = r"(\+|-){2,}"

    recipient_id, plus_or_minus = None, None

    for substring in re.split(PATTERN, string):

        if re.match(recipient_id_pattern, substring):

            recipient_id = substring

        elif re.match(plus_or_minus_pattern, substring):

            assert recipient_id, "should not have points without a recipient"

            plus_or_minus = substring

        if recipient_id and plus_or_minus:

            yield recipient_id, plus_or_minus

            recipient_id, plus_or_minus = None, None


@slack_events_adapter.on("app_mention")
def app_mention(event_data):

    logger.bind(event_data=event_data).info("app mention")

    event_text = event_data["event"]["text"]

    channel = event_data["event"]["channel"]

    if any(s in event_text.lower() for s in ("lead", "loser")):

        is_leaderboard = "lead" in event_text.lower()

        logger.info("showing leaders" if is_leaderboard else "showing losers")

        users = [
            User(**row)
            for row in user_table.all(
                order_by="-karma" if is_leaderboard else "karma", _limit=10
            )
        ]

        result = []

        for index, user in enumerate(users, start=1):

            line = f"{index}. *{user.display_name}* `{user.karma}`"

            result.append(line)

        text = "\n".join(result)

        logger.info("posting leaders (or losers) message")

        post_message_kwargs = dict(
            text=text,
            channel=channel,
            blocks=[
                {
                    "type": "section",
                    "text": {
                        "type": "mrkdwn",
                        "text": text,
                    },
                },
            ],
        )

        event = event_data["event"]

        if "thread_ts" in event:
            post_message_kwargs.update(thread_ts=event["thread_ts"])

        client.chat_postMessage(**post_message_kwargs)

        logger.info("leaders (or losers) message posted")


@slack_events_adapter.on("message")
def message(event_data):

    logger.bind(event_data=event_data).info("message received")

    event = event_data["event"]

    channel = event["channel"]

    text = event["text"]

    timestamp = event_data["event_time"]

    match = PATTERN.search(text)

    if match:

        logger.info("message matched")

        if short_circuit(timestamp):

            logger.error("already executed. short circuiting")

            raise SystemExit()

        recipients = list(split(text))

        logger.bind(recipients=recipients).info("iterating recipients")

        for recipient_id, plus_or_minus in recipients:

            delta = len(plus_or_minus) // 2

            if plus_or_minus.startswith("-"):

                delta = 0 - delta

            user_info = client.users_info(user=recipient_id).data["user"]

            logger.bind(user_info=user_info).info("fetched user info")

            name = user_info["name"]

            user: User = upsert_user(recipient_id, name, delta)

            logger.bind(user=user).info("upserted")

            positive_delta = delta > 0

            wins = [
                "baby-yoda",
                "spiderman",
                "smart-move",
                "shiblob-reach",
                "nyan_cat",
                "pickle-rick",
                "xzibit",
                "shocked",
            ]

            fails = [
                "crycat",
                "facepalm_gif",
                "poop",
                "shocked",
                "shrugs",
                "excuseme",
            ]

            emoji = (
                random.choice(wins) if positive_delta else random.choice(fails)
            )

            client.reactions_add(
                name=emoji, timestamp=event["ts"], channel=channel
            )

            text = f"@{user.display_name} totals *{user.karma} karma points*"

            logger.info("posting point message")

            post_message_kwargs = dict(
                text=text,
                channel=channel,
                blocks=[
                    {
                        "type": "section",
                        "text": {
                            "type": "mrkdwn",
                            "text": text,
                        },
                    },
                ],
            )

            if "thread_ts" in event:

                post_message_kwargs.update(thread_ts=event["thread_ts"])

            client.chat_postMessage(**post_message_kwargs)

            logger.info("point message posted")


@retry(stop=stop_after_attempt(5), wait=wait_fixed(2))
def upsert_user(id: str, display_name: str, delta: int) -> User:

    logger.bind(id=id, display_name=display_name, delta=delta).info(
        "upserting"
    )

    existing_user: Optional[dict] = user_table.find_one(id=id)

    existing_user: Optional[User] = (
        User(**existing_user) if existing_user else None
    )

    logger.bind(existing_user=existing_user).info("fetched existing user")

    old_karma = existing_user.karma if existing_user else 0

    karma = old_karma + delta

    logger.bind(
        existing_user=existing_user,
        old_karma=old_karma,
        delta=delta,
        new_karma=karma,
    ).info("updating karma")

    user: User = User(id=id, display_name=display_name, karma=karma)

    logger.bind(user=user.dict()).info("inserting user")

    user_table.upsert(user.dict(), ["id"])

    logger.bind(user=user.dict()).info("user inserted")

    return user


def short_circuit(timestamp: str):

    executed = timestamp_table.find_one(timestamp=timestamp)

    if executed:

        logger.bind(executed=executed).warning("executed")

        return True

    else:

        timestamp_table.insert(dict(timestamp=timestamp))

        logger.info("not executed")

    return False


if __name__ == "__main__":
    app.run(debug=True, port=5001)
