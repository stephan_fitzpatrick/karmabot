import json
import os
from types import SimpleNamespace
from typing import Optional

import boto3
from pydantic import BaseSettings


class Config(BaseSettings):

    cluster_arn: Optional[str]
    cluster_secret_arn: Optional[str]

    echo_db: bool = False

    database: str = "karmabot"

    @property
    def slack(_):

        access_token, signing_secret = (
            os.getenv(v)
            for v in ("SLACK_ACCESS_TOKEN", "SLACK_SIGNING_SECRET")
        )

        if not (access_token and signing_secret):

            boto3_session = boto3.Session()

            ssm_client = boto3_session.client("ssm")

            slack_secrets = json.loads(
                ssm_client.get_parameter(
                    Name="/karmabot/slack",
                    WithDecryption=True,
                )["Parameter"]["Value"]
            )

            access_token, signing_secret = (
                slack_secrets[k] for k in ("access_token", "signing_secret")
            )

        return SimpleNamespace(
            access_token=access_token,
            signing_secret=signing_secret,
            endpoint="/slack/events",
        )

    @property
    def vpc_id(self):

        return os.environ["VPC_ID"]
