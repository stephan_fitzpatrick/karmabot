from typing import Optional

from aws_cdk import aws_apigateway as apigw
from aws_cdk import aws_ec2 as ec2
from aws_cdk import aws_lambda, aws_lambda_python
from aws_cdk import aws_rds as rds
from aws_cdk import aws_ssm as ssm
from aws_cdk import core
from lambdas.karmabot.config import Config

config = Config(cluster_arn=None, cluster_secret_arn=None)

class Network(core.NestedStack):

    vpc: ec2.Vpc

    def __init__(self, scope, id, vpc: Optional[ec2.Vpc] = None):

        super().__init__(scope, id)

        self.vpc = (
            vpc
            if vpc is not None
            else ec2.Vpc.from_lookup(
                self,
                "karmabotVpc",
                vpc_id=config.vpc_id,
            )
        )


class Persistence(core.NestedStack):

    cluster: rds.ServerlessCluster

    def __init__(self, scope, id, vpc: ec2.Vpc):

        super().__init__(scope, id)

        self.aurora_cluster = rds.ServerlessCluster(
            self,
            "karmabotAuroraCluster",
            engine=rds.DatabaseClusterEngine.AURORA_MYSQL,
            scaling=rds.ServerlessScalingOptions(
                min_capacity=rds.AuroraCapacityUnit.ACU_1
            ),
            vpc=vpc,
            enable_data_api=True,
            default_database_name=config.database,
            removal_policy=core.RemovalPolicy.DESTROY,
        )


class Compute(core.NestedStack):
    def __init__(self, scope, id, cluster: rds.ServerlessCluster):

        super().__init__(scope, id)

        fn = aws_lambda_python.PythonFunction(
            self,
            "karmaBotFn",
            entry="./lambdas/karmabot",
            runtime=aws_lambda.Runtime.PYTHON_3_8,
            environment={
                "CLUSTER_ARN": cluster.cluster_arn,
                "CLUSTER_SECRET_ARN": cluster.secret.secret_arn,
            },
            timeout=core.Duration.seconds(15),
            dead_letter_queue_enabled=True,
        )

        slack_secrets = (
            ssm.StringParameter.from_secure_string_parameter_attributes(
                self,
                "slackSecrets",
                version=1,
                parameter_name="/karmabot/slack",
            )
        )

        slack_secrets.grant_read(fn)

        cluster.grant_data_api_access(fn)

        apigw.LambdaRestApi(
            self,
            "karmaBotLambdaRestApi",
            handler=fn,
        )


class KarmabotStack(core.Stack):
    def __init__(self, scope: core.Construct, construct_id: str, **kwargs):

        super().__init__(scope, construct_id, **kwargs)

        network_stack = Network(self, "karmabotNetwork")

        persistence_stack = Persistence(
            self, "karmabotPersistence", network_stack.vpc
        )

        compute_stack = Compute(
            self, "karmabotCompute", persistence_stack.aurora_cluster
        )

        core.CfnOutput(
            self,
            "clusterArn",
            value=persistence_stack.aurora_cluster.cluster_arn,
        )

        core.CfnOutput(
            self,
            "secretArn",
            value=persistence_stack.aurora_cluster.secret.secret_arn,
        )


class Karmabot(core.Construct):
    def __init__(self, scope, id, **kwargs):
        super().__init__(scope, id)
        karmabot_stack = KarmabotStack(self, "karmabotStack", **kwargs)
