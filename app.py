#!/usr/bin/env python3

import os

from aws_cdk import core

from karmabot.karmabot_stack import Karmabot

app = core.App()

Karmabot(
    app,
    "karmabot",
    env={
        "account": os.getenv("CDK_DEFAULT_ACCOUNT"),
        "region": os.getenv("CDK_DEFAULT_REGION"),
    },
)

app.synth()
